<!doctype html public "-//w3c//dtd html 4.0 transitional//en">

<html>
<head>

  <title>GeoMapping</title>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

  <!-- Style Sheets -->
  <link rel=stylesheet href="../../Stylesheets/styles.css" type="text/css">
  <link rel="stylesheet" type="text/css" href="../Styles/menuH.css">
  <link rel="stylesheet" type="text/css" href="../../Stylesheets/menuH2.css">

  <style type="text/css">

  <!-- Styles for body of the page-->
  	body {
		background-color:white;
		font-family:Frosty;
		color:black;
  	}

  	<!-- Some styles for paragraphs and links-->
  	p.ex1 {
		font-family:Frosty;
  	}

  	a:hover {color:#6699CC;}

  	a {text-decoration:none; color:#0000A0;}
  	input[type='text'] { font-size: 16px; }
  	input[type='file'] { font-size: 16px; }
  	input[type='submit'] { font-size: 16px; }

   </style>

</head>

<!-- Generate body style -->
<body text="#000000" bgcolor="#FFFFFF" link="#0000FF" vlink="#990000" alink="#FF0000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
&nbsp;

<!-- Create banner -->
<br>
<div style="width: 100%; height: 440px; margin-left:auto; margin-right:auto; text-align: center;">
<img src="../Images/japan_zen.jpg" style="width: 1200; height: 440; float:inherited;" />
</div>
<br>
